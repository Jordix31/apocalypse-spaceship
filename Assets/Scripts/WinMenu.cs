﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinMenu : MonoBehaviour {

	private void Start(){
		Time.timeScale = 1;
	}
	public void Restart()
	{
		Debug.Log("Restart");
		SceneManager.LoadScene("MainMenu");
	}
	public void Exit()
	{
		Debug.Log("Exit");
		Application.Quit();
	}
}
