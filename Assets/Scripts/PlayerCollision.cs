﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour {
	public MyGameManager Life;
	public AudioSource audioDamage;

	private void OnTriggerEnter (Collider cool){
		if (cool.tag == "Enemy") {
			Life.LoseLive ();	
			audioDamage.Play ();
			}
	} 

}
