﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {
	public Text livesText;
	public int lives;

	void Start () {
		lives = 3;
		//livesText.text = "x3";
		livesText.text = "x " + lives.ToString ();
	}

	public void LoseLive(){
		lives--;
		livesText.text = "x " + lives.ToString ();
		if (lives < 0) {
			SceneManager.LoadScene("GameOver");
			Cursor.visible = true;
		}
	}
}