﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreeperGenerator : MonoBehaviour {

	public GameObject ZombieWalk;
	public Transform[] points;
	public float timeToSpawn;
	public Transform pointtospawn;
	IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);

			GameObject izombiewalk = Instantiate (ZombieWalk,pointtospawn);
			CreeperBehaviour cb = izombiewalk.GetComponent<CreeperBehaviour> ();
			cb.pathNodes = points;
		}
	}
}
