﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scriptTimer : MonoBehaviour {

	public Text Tiempo;
	private float tiempo = 300f;

	// Use this for initialization
	void Start () {
		Tiempo.text = " " + tiempo;
	}
	
	// Update is called once per frame
	void Update () {
        tiempo -= Time.deltaTime;
		Tiempo.text = " " + tiempo.ToString("f0");

        if(tiempo <= 0)
        {
			Tiempo.text = "0";
        }
	
	}
}
